class SessionsController < ApplicationController

  def new
    if current_user
      redirect_to root_path
    end
  end

  def create
    user = User.find_by(email: params[:email].downcase)
    if user && user.authenticate(params[:password])
      log_in user
      params[:remember_me] == '1' ? remember(user) : forget(user)
      redirect_back_or user
      #redirect_to blogs_path, notice: "Zostałeś zalogowany"
      #redirect_to user, notice: "Zostałeś zalogowany"
    else
      redirect_to login_url, alert: "Hasło lub emeil są nieprawidłowe"
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end
