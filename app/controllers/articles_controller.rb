class ArticlesController < ApplicationController
  before_action :logged_in_user, except: [:show]

  def index
    @article = Article.all

    respond_to do |format|
      format.html
      format.pdf do
        pdf = Article_Pdf.new(@article)
        send_data pdf.render, filename: 'article.pdf', type: 'application/pdf', disposition: "inline"
      end
    end
  end

  def show
    @article = Article.find(params[:id])
  end

  def new
    @article = Article.new
  end

  def create
    @blog = Blog.find(params[:blog_id])
    @article = @blog.articles.create(article_params)
    if @blog.save
      redirect_to blog_path(@article.blog_id)
    else
      render 'new'
    end
  end

  def edit
    @article = Article.find(params[:id])
  end

  def update
    @article = Article.find(params[:id])
    if @article.update(article_params)
      redirect_to blog_path(@article.blog_id)
    else
      render 'edit'
    end
  end

  def destroy
    @article = Article.find(params[:id])
    @article.destroy
    redirect_to blog_path(@article.blog_id)
  end

  def logged_in_user
    unless logged_in?
      flash[:danger] = "Please log in."
      redirect_to login_url
    end
  end

  private
    def article_params
      params.require(:article).permit(:title, :text, :image, :tag_list)
    end
end
