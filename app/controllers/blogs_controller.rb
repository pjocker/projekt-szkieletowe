class BlogsController < ApplicationController
  before_action :logged_in_user, except: [:show, :index]
    def show
      @blog = Blog.find(params[:id])
      @article = Article.all
    end

    def new
      @blog = Blog.new
    end

    def create
      @user = current_user
      @blog = @user.blogs.create(blog_params)
      if @blog.save(blog_params)
        redirect_to blog_path(@blog)
      else
        render 'new'
      end
    end

    def edit
      @blog = Blog.find(params[:id])
    end

    def update
      @blog = Blog.find(params[:id])
      if @blog.update(blog_params)
        redirect_to blog_path(@blog)
      else
        render 'edit'
      end
    end

    def index
      @blog = Blog.all

      respond_to do |format|
        format.html
        format.pdf do
          pdf = Blog_Pdf.new
          send_data pdf.render, filename: 'blog.pdf', type: 'application/pdf', disposition: "inline"
        end
      end
    end

    def destroy
      @blog = Blog.find(params[:id])
      @blog.destroy
      redirect_to blogs_path
    end

  def logged_in_user
    unless logged_in?
      flash[:danger] = "Please log in."
      redirect_to login_url
    end
  end

    private
    def blog_params
      params.require(:blog).permit(:title)
    end
end

