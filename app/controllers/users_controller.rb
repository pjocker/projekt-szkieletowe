class UsersController < ApplicationController
  before_action :logged_in_user, except: [:new, :create, :all]
  def index
    render 'show'
    end
  def all
    @user = User.all

    respond_to do |format|
      format.html
      format.pdf do
        pdf = Users_pdf.new(@user)
        send_data pdf.render, filename: 'user.pdf', type: 'application/pdf', disposition: "inline"
      end
    end
  end
  def show
    @user = User.find(params[:id])
  end

  def new
    if current_user
      redirect_to root_path
    else
      @user = User.new
    end
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash[:success] = "Welcome !"
      log_in @user
      redirect_to blogs_path
      #redirect_to @user
    else
      render 'new'
    end
  end

  def edit
    @user = current_user
  end

  def update
    @user = current_user
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end

  def destroy
    current_user.destroy
    flash[:success] = "User deleted"
    redirect_to root_path
  end

  # Potwierdzenie zalogowanego użytkownika.
  def logged_in_user
    unless logged_in?
      flash[:danger] = "Please log in."
      redirect_to login_url
    end
  end
  private
  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
end
