class Article_Pdf < Prawn::Document
def initialize(article)
    super()
    @article = article
    article_id
  end

  def article_id
    table article_id_all do
      row(0).font_style = :bold
      columns(1..3).align = :right
      self.row_colors = ["DDDDDD", "FFFFFF"]
      self.header = true
    end
  end

  def article_id_all
    [["ID","Title","Text","Blog"]] +
        @article.map do |articles|
          [articles.id, articles.title, articles.text, Blog.find(articles.blog_id).title]
        end
  end
end