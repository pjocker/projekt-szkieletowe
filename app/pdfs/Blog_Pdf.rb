class Blog_Pdf < Prawn::Document
  def initialize()
    super()
    @blog = Blog.all
    article_id
  end

  def article_id
    table article_id_all do
      row(0).font_style = :bold
      columns(1..3).align = :right
      self.row_colors = ["DDDDDD", "FFFFFF"]
      self.header = true
    end
  end

  def article_id_all
    [["ID","Title", "User"]] +
        @blog.map do |blogs|
          [blogs.id, blogs.title, User.find(blogs.user_id).email]
        end
  end
end