class Users_pdf < Prawn::Document
def initialize(user)
    super()
    @user = user
    user_id
  end

  def user_id
    table user_id_all do
      row(0).font_style = :bold
      columns(1..3).align = :right
      self.row_colors = ["DDDDDD", "FFFFFF"]
      self.header = true
    end
  end

  def user_id_all
    [["ID","Name","Email"]] +
        @user.map do |users|
          [users.id, users.name, users.email]
        end
  end
end