class Article < ApplicationRecord
  belongs_to :blog
  has_many :comments, dependent: :destroy
  acts_as_taggable_on :tags
  has_attached_file :image, :styles => { :original => '450x450', :thumb => '50x50', :good => '453x453' },:default_url => "pobrane.jpg"
  validates_attachment :image, presence => true,
                       :content_type => {:content_type => ["image/jpeg", "image/jpg", "image/png"]}
end
