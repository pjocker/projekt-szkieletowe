module SessionsHelper
  # Loguje użytkownika
  def log_in(user)
    session[:user_id] = user.id
  end
  # Zwraca użytkownika odpowiadającego plikowi cookie
  def current_user
    if (user_id = session[:user_id])
      @current_user ||= User.find_by(id: user_id)
    elsif (user_id = cookies.signed[:user_id])
      user = User.find_by(id: user_id)
      if user && user.authenticated?(cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
  end
  # Zwraca true jeśli dany użytkownik jest zalogowany
  def current_user?(user)
    user == current_user
  end
  def logged_in?
    !current_user.nil?
  end
  # Zapamiętuje użytkownika w sesji
  def remember(user)
    user.remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end
  # Zapomina o sesji
  def forget(user)
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end

  # Wylogowuje
  def log_out
    forget(current_user)
    session.delete(:user_id)
    @current_user = nil
  end

  # Przekierowuje do zapisanej lokalizacji
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  # Przechowuje adres URL próbujący uzyskać dostęp
  def store_location
    session[:forwarding_url] = request.original_url if request.get?
  end
end
