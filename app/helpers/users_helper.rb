module UsersHelper
  def gravatar_for(size: 80)
    gravatar_id = Digest::MD5::hexdigest(current_user.email.downcase)
    gravatar_url = "https://s.gravatar.com/avatar/6764dfd61b4e991d6ca0a6fbe88d9a07?s=#{size}"
    image_tag(gravatar_url, alt: current_user.name, class: "gravatar")
  end
end
