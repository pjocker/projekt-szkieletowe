Rails.application.routes.draw do
  get 'comments/create'

  get 'comments/show'

  get 'comments/destroy'

  get 'articles/index'

  root 'public_controller#home'
  get '/contact', to: 'public_controller#contact'
  get    '/all',  to: 'users#all'
  get    '/signup',  to: 'users#new'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  get '/edit',  to: 'users#edit'
  get '/delete',  to: 'users#destroy'
  resources :users
  resources :blogs do
    resources :articles
  end
  resources :articles do
    resources :comments
  end
  resources :tags, only: [:index, :show]
end
