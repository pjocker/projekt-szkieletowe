require 'test_helper'

class PublicControllerControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get public_controller_home_url
    assert_response :success
  end

end
